// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAi2-3_FiNwQEHbyW7xRUl3LvtFocmMnYQ',
    authDomain: 'proyecto-67055.firebaseapp.com',
    databaseURL: 'https://proyecto-67055.firebaseio.com',
    projectId: 'proyecto-67055',
    storageBucket: 'proyecto-67055.appspot.com',
    messagingSenderId: '607298870671',
    appId: '1:607298870671:web:2b21a5f873ca09c3107ffa',
    measurementId: 'G-KPRFDEX3MM'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
