import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MedicoService } from 'src/app/services/medico.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-citas-medico',
  templateUrl: './citas-medico.page.html',
  styleUrls: ['./citas-medico.page.scss'],
})
export class CitasMedicoPage implements OnInit {

  citas: Observable<any[]>;

  constructor(private medicoService: MedicoService, public router: Router) { }

  ngOnInit() {
    this.citas=this.medicoService.getCitas();
  }
  
  showCita(id: any)
  {
    this.router.navigate([`cita-medico/${id}`])
  }

  trackByFn(index, obj) {
    return obj.uid;
  }

  showCrearCita()
  {
    this.router.navigate(['crear-cita-medico'])
  }
}
