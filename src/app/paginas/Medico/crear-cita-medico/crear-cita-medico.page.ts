import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MedicoService } from 'src/app/services/medico.service';
import { citaMedico } from 'src/app/shared/citaMedico.class';

@Component({
  selector: 'app-crear-cita-medico',
  templateUrl: './crear-cita-medico.page.html',
  styleUrls: ['./crear-cita-medico.page.scss'],
})
export class CrearCitaMedicoPage implements OnInit {

  cita: citaMedico = new citaMedico()

  constructor(public router: Router,private medicoService: MedicoService) { }

  ngOnInit() {
  }

  guardarCita(){
    //console.log(this.empleo)
    this.medicoService.saveCita(this.cita);
    this.router.navigate(['citas-medico']);
  }
}
