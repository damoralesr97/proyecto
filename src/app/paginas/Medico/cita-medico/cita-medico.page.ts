import { Component, OnInit } from '@angular/core';
import { MedicoService } from 'src/app/services/medico.service';
import { Router, ActivatedRoute } from '@angular/router';
import { citaMedico } from 'src/app/shared/citaMedico.class';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cita-medico',
  templateUrl: './cita-medico.page.html',
  styleUrls: ['./cita-medico.page.scss'],
})
export class CitaMedicoPage implements OnInit {

  cita: Observable<any>
  citaUpdate: citaMedico = new citaMedico()

  constructor(public router: Router,private medicoService: MedicoService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id")
    this.cita = this.medicoService.getCita(id)
    this.cita.subscribe(data => {this.citaUpdate=data;})
  }

  actualizaCita()
  {
    this.medicoService.saveCita(this.citaUpdate);
    this.router.navigate(['citas-medico']);
    
  }

}
