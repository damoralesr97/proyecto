import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { User } from '../../shared/user.class';
import { FirestoreService } from 'src/app/services/firestore.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user: User = new User();

  constructor(private authSvs: AuthService, private router: Router, private firestoreService: FirestoreService) { }

  ngOnInit() {}

  async onLogin() {
    const user = await this.authSvs.onLogin(this.user);
    if (user){
      this.firestoreService.getUsuario(user.user.uid).subscribe(usuario => {
        if ( usuario.rol === 'doctor' ) {
          this.router.navigateByUrl('inicio-medico');
        } else if (usuario.rol === 'admin') {
          this.router.navigateByUrl('inicio-admin');
        } else if (usuario.rol === 'user') {
          this.router.navigateByUrl('inicio-user');
        }
      });
    }
  }

}
