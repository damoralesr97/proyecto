import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListaCitasPage } from './lista-citas.page';

describe('ListaCitasPage', () => {
  let component: ListaCitasPage;
  let fixture: ComponentFixture<ListaCitasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCitasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListaCitasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
