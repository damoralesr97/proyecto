import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { FirestoreService } from '../../services/firestore.service';
import { User } from '../../shared/user.class';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  user: User = new User();

  constructor(private authSvs: AuthService, private router: Router, private firestoreService: FirestoreService) { }

  ngOnInit() {
  }

  async onRegister(){
    const user = await this.authSvs.onRegister(this.user);
    if (user) {
      this.user.email = this.user.email;
      this.user.rol = 'user';
      this.user.uid = user.user.uid;
      this.firestoreService.insertar('usuarios', this.user);
      console.log('Usuario creado correctamente');
      this.router.navigateByUrl('/login');
    }
  }

}
