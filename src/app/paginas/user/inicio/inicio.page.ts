import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirestoreService } from '../../../services/firestore.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  user: Observable<any[]>;

  // tslint:disable-next-line: max-line-length
  constructor(private menuCtrl: MenuController, private authSvc: AuthService, private router: Router, private afAuth: AngularFireAuth, private firestoreService: FirestoreService) { }

  ngOnInit() {
    this.user = this.firestoreService.getUsuario(this.authSvc.isLoged.uid);
    this.user.subscribe(user => console.log(user));
  }

  logout(){
    console.log('Logout!');
    this.afAuth.signOut();
    this.router.navigateByUrl('/login');
  }

}
