import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearCitaUserPageRoutingModule } from './crear-cita-user-routing.module';

import { CrearCitaUserPage } from './crear-cita-user.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrearCitaUserPageRoutingModule
  ],
  declarations: [CrearCitaUserPage]
})
export class CrearCitaUserPageModule {}
