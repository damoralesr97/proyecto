import { Component, OnInit } from '@angular/core';
import { Cita } from '../../../shared/cita.class';
import { AuthService } from '../../../services/auth.service';
import { Observable } from 'rxjs';
import { FirestoreService } from '../../../services/firestore.service';
import { User } from '../../../shared/user.class';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-cita-user',
  templateUrl: './crear-cita-user.page.html',
  styleUrls: ['./crear-cita-user.page.scss'],
})
export class CrearCitaUserPage implements OnInit {

  cita = new Cita();
  paciente: string;
  medicos: Observable<any>;


  constructor(private authSvc: AuthService, private firestoreService: FirestoreService, private router: Router) { }

  ngOnInit() {
    this.authSvc.afAuth.authState.subscribe( user => (this.paciente = user.uid));
    this.medicos = this.firestoreService.getMedicos();
  }

  onSubmitTemplate(){
    this.cita.pacienteUid = this.paciente;
    console.log(this.cita);
    this.firestoreService.guardarCita('citas', this.cita);
    this.router.navigateByUrl('/citas-user');
  }

  trackByFn(index, obj) {
    return obj.uid;
  }

}
