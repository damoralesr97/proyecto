import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearCitaUserPage } from './crear-cita-user.page';

describe('CrearCitaUserPage', () => {
  let component: CrearCitaUserPage;
  let fixture: ComponentFixture<CrearCitaUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearCitaUserPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearCitaUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
