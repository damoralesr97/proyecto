import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearCitaUserPage } from './crear-cita-user.page';

const routes: Routes = [
  {
    path: '',
    component: CrearCitaUserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearCitaUserPageRoutingModule {}
