import { Component, OnInit } from '@angular/core';
import { FirestoreService } from '../../../services/firestore.service';
import { Observable } from 'rxjs';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-citas',
  templateUrl: './citas.page.html',
  styleUrls: ['./citas.page.scss'],
})
export class CitasPage implements OnInit {

  paciente;
  citasPendientes: Observable<any[]>;
  citasProgramadas: Observable<any[]>;

  constructor(private authSvc: AuthService, private firestoreService: FirestoreService) { }

  ngOnInit() {
    this.paciente = this.authSvc.isLoged;
    this.citasPendientes = this.firestoreService.getCitasPendientes(this.paciente);
  }

  trackByFn(index, obj) {
    return obj.uid;
  }

}
