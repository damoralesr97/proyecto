import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./paginas/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./paginas/registro/registro.module').then( m => m.RegistroPageModule)
  },
  {
    path: 'inicio-user',
    loadChildren: () => import('./paginas/user/inicio/inicio.module').then( m => m.InicioPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'inicio-medico',
    loadChildren: () => import('./paginas/Medico/inicio-medico/inicio-medico.module').then( m => m.InicioMedicoPageModule)
  },
  {

    path: 'citas-user',
    loadChildren: () => import('./paginas/user/citas/citas.module').then( m => m.CitasPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'citas-medico',
    loadChildren: () => import('./paginas/Medico/citas-medico/citas-medico.module').then( m => m.CitasMedicoPageModule)
  },
  {
    path: 'cita-medico/:id',
    loadChildren: () => import('./paginas/Medico/cita-medico/cita-medico.module').then( m => m.CitaMedicoPageModule)
  },
  {
    path: 'crear-cita-medico',
    loadChildren: () => import('./paginas/Medico/crear-cita-medico/crear-cita-medico.module').then( m => m.CrearCitaMedicoPageModule)
  },
  {
    path: 'crear-cita-user',
    loadChildren: () => import('./paginas/user/crear-cita-user/crear-cita-user.module').then( m => m.CrearCitaUserPageModule)
  },
  {
    path: 'lista-medicos',
    loadChildren: () => import('./paginas/administrador/lista-medicos/lista-medicos.module').then( m => m.ListaMedicosPageModule)
  },
  {
    path: 'lista-citas',
    loadChildren: () => import('./paginas/administrador/lista-citas/lista-citas.module').then( m => m.ListaCitasPageModule)
  },
  {
    path: 'lista-medicamentos',
    loadChildren: () => import('./paginas/administrador/lista-medicamentos/lista-medicamentos.module').then( m => m.ListaMedicamentosPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
