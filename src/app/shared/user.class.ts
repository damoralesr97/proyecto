export class User {
    uid: string;
    cedula: string;
    nombres: string;
    apellidos: string;
    telefono: string;
    email: string;
    password: string;
    rol: string;
    avatar: string;
}
