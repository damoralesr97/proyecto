import { Timestamp } from 'rxjs';
import { Time } from '@angular/common';

export class Cita {
    uid: string;
    pacienteUid: string;
    medicoUid: string;
    fecha: Date;
    hora: Time;
    observaciones: string;
    estado: string;
}
