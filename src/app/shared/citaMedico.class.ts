import { Timestamp } from 'rxjs';
import { Time } from '@angular/common';

export class citaMedico {
    uid: string;
    cedula: string;
    nombre: string;
    fecha: string;
    hora: string;
    telefono: string;
    sintomas: string;
}
