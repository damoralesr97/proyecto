import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Cita } from '../shared/cita.class';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(private angularFirestore: AngularFirestore) { }

  // **************** Metodos para USER ****************

  public insertar(coleccion, datos) {
    datos.avatar = 'https://firebasestorage.googleapis.com/v0/b/proyecto-67055.appspot.com/o/profile%2Fuser.png?alt=media&token=00a65435-167e-4098-a793-67677e63f5f0';
    const param = JSON.parse(JSON.stringify(datos));
    return this.angularFirestore.collection(coleccion).doc(datos.uid).set(param);
  }

  public getUsuario(uid: string): Observable<any>{
    const itemDoc = this.angularFirestore.doc<any>(`usuarios/${uid}`);
    return itemDoc.valueChanges();
  }

  public getMedicos(): Observable<any[]>{
    return this.angularFirestore.collection('usuarios',
      ref => ref.where('rol', '==', 'doctor')).valueChanges();
  }

  public guardarCita(coleccion, cita: Cita) {
    cita.uid = this.angularFirestore.createId();
    cita.estado = 'Pendiente';
    const param = JSON.parse(JSON.stringify(cita));
    return this.angularFirestore.collection(coleccion).doc(cita.uid).set(param, {merge: true});
  }

  public getCitasPendientes(paciente: any): Observable<any[]>{
    //const param = JSON.parse(JSON.stringify(paciente));
    //console.log(param);
    return this.angularFirestore.collection('citas',
      ref => ref.where('estado', '==', 'Pendiente').where('pacienteUid', '==', paciente.uid)).valueChanges();
  }

  // ***************************************************

}
