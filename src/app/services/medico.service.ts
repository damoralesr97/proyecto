import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { citaMedico } from '../shared/citaMedico.class';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  constructor(private angularFirestore: AngularFirestore) { }

  //------------------------------CITAS---------------------------------

  public saveCita(cita: citaMedico)
  {
    const refCita = this.angularFirestore.collection("citasMedico")
    if(cita.uid==null)
    {
      cita.uid= this.angularFirestore.createId()
    }
    const param = JSON.parse(JSON.stringify(cita));
    refCita.doc(cita.uid).set(param, {merge: true})

  }

  public getCita(uid: string): Observable<any>{
    const itemDoc = this.angularFirestore.doc<any>(`citasMedico/${uid}`);
    return itemDoc.valueChanges();
  }

  getCitaNombre(nombre: string): Observable<any[]>
  {
    return this.angularFirestore.collection('citasMedico', 
      ref => ref.where("nombre","==",`${nombre}`)
                .orderBy('fecha', 'asc')).valueChanges();
  }

  public getCitas(): Observable<any[]>{
    return this.angularFirestore.collection('citasMedico',
      ref => ref.orderBy('fecha', 'asc')).valueChanges();
       
  }
  
  //--------------------------------------------------------------------
}
